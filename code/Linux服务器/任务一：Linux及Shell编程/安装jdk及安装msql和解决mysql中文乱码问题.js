 1.安装jdk
   
   系统默认安装OpenJDK，首先需要删除OpenJDK
   1).查看以前是不是安装了openjdk
   如果不是root用户需要切换到root用户(su - root)
   命令：rpm -qa | grep java
   显示如下：(有则卸载，没有就不用)，注意版本可能会有些不一样，以实际操作的为准
   tzdata-java-2013g-1.el6.noarch
   java-1.7.0-openjdk-1.7.0.45-2.4.3.3.el6.x86_64
   java-1.6.0-openjdk-1.6.0.0-1.66.1.13.0.el6.x86_64
   2).卸载openjdk
   (其中参数"tzdata-java-2013j-1.el6.noarch"为上面查看中显示的结果，粘进来就行，如果你显示的不一样，请复制你查询到的结果)
   rpm -e --nodeps java-1.6.0-openjdk-1.6.0.0-1.66.1.13.0.el6.i686
   rpm -e --nodeps java-1.7.0-openjdk-1.7.0.45-2.4.3.3.el6.i686
   rpm -e --nodeps tzdata-java-2013g-1.el6.noarch
   -e(erase) -- package(uninstall)移除包(卸载)
   --nodeps do not verify package dependencies -- 不要验证包依赖关系
   3).安装jdk
   (1)切换到root用户并进入usr目录：cd /usr
   (2)在usr目录下创建java文件夹：mkdir java
   (3)将jdk-7u71-linux-x64.tar.gz拷贝或上传到java目录下(也可以用工具)
   (4)进入/usr/java文件夹下：cd /usr/java/
   (5)修改权限，参数"jdk-7u71-linux-x64.tar.gz"为你自己上传的jdk安装文件
   chmod 755 jdk-7u71-linux-x64.tar.gz
   (6)解压：tar –zxvf jdk-7u71-linux-x64.tar.gz
   (7)配置环境变量
    vi /etc/profile
	添加内容
	export 主要用来设置环境变量
	export JAVA_HOME=/usr/java/jdk1.8.0_231/
	export CLASSPATH=.:${JAVA_HOME}/jre/lib/rt.jar:${JAVA_HOME}/lib/dt.jar:${JAVA_HOME}/lib/tool.jar
	export PATH=$PATH:${JAVA_HOME}/bin
   (8)重新编译环境变量
   source /etc/profile
   (9)测试
   java -version
 
 2.安装tomcat
   
   1).创建安装目录：mkdir /usr/tomcat
   2).给apache-tomcat-7.0.57.tar.gz 文件权限
   3).解压tomcat压缩文件：
   tar -zxvf apache-tomcat-7.0.57.tar.gz -C /usr/tomcat
   注意，这里也可以只打开tomcat所需端口：8080
   4)启动tomcat：
   进入tomcat的bin目录：cd usr/tomcat/apache-tomcat-7.0.57/bin/
   启动tomcat web服务器: ./startup.sh
   访问：192.168.72.100:8080
   5)停止tomcat
   ./shutdown.sh
   6)查看tomcat日志信息
   tail -200f /usr/tomcat/apache-tomcat-7.0.57/logs/catalina.out
   200 表示最后显示行数
   也可以用组合命令，启动并查看日志：
   进入tomcat的bin目录
   ./startup.sh && tail -200f ../logs/catalina.out
   
 3.安装mysql
   
   1)查询系统自带的mysql
   rpm -qa | grep mysql
   mysql-libs-5.1.73-8.el6_8.x86_64
   2)卸载系统自带的mysql
   rpm -e --nodeps mysql-libs-5.1.73-8.el6_8.x86_64
   3).下载安装官网yum源
   查看yum仓库
   ll /etc/yum.repos.d/
   下载yum源
   wget -P /usr/software http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm
   yum install wget
   需要联网
   4).安装下载好的rpm文件
   cd /usr/software
   rpm -ivh mysql-community-release-el6-5.noarch.rpm
   5).安装mysql服务器
   yum -y install mysql-community-server
   需要联网下载，估计需要等待十几分钟
   测试是否安装成功
   rpm -qa | grep mysql
   6).启动服务
   service mysqld start
   如果出现:serivce:command not found
   安装 service
   yum install initscripts
   7).修改密码
   /usr/bin/mysqladmin -u root password '123'
   mysql -uroot -p123
   8).解决中文乱码
   解决办法：修改MySQL数据库字符编码为UTF-8，UTF-8 包含全世界所有国家需要用到的字符,是国际编码。
   具体操作:
   进入mysql控制台
   mysql -uroot -p123   -- 进入mysql
   show variables like 'character_set_%';  -- 查看编码集 发现不是utf8
   修改配置文件
   [root@localhost ~]# >/etc/mycnf
   [root@localhost ~]# vi /etc/my.cnf
   修改内容如下
   [client]
   default-character-set=utf8
   
   [mysql]
   default-character-set=utf8
   
   [mysqld]
   character-set-server=utf8
   重启mysql服务
   [root@localhost ~]# service mysqld restart
   
   mysql -uroot -p123   -- 进入mysql
   show variables like 'character_set_%';  -- 查看编码集 发现是utf8
 
 4.默认情况下mysql服务端不允许客户端远程访问
      
   问题:远程连接mysql报错
   原因:因为用户没有远程访问的权限
   给root用户授权：既可以访问，也可以远程访问
   grant all privileges on *.* to 'root'@'%' identified by '123' with grant option;
   刷新权限
   flush privileges;
   
   
 
   
   
   
   
   
   
   

