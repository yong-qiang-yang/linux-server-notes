 1.ssh协议[安全外壳协议]
  
  SSH为Secure Shell的缩写，由IETF的网络小组（Network Working Group）所制定；SSH为建立在应用层基础上的安全协议
。SSH是较可靠，专为远程登录会话和其他网络服务提供安全性的协议。
  利用SSH协议可以有效防止远程管理过程中的信息泄露问题。
  SSH提供两种级别的安全验证：
  第一种级别(基于口令的安全验证)
  只要你知道自己帐号和口令，就可以登录到远程主机。所有传输的数据都会被加密，但是不能保证你正在连接的服务器就是
你想连接的服务器。可能会有别的服务器在冒充真正的服务器，也就是受到"中间人"这种方式的攻击。
  第二种级别(基于密钥的安全验证)
  需要依靠密匙，也就是你必须为自己创建一对密匙，并把公用密匙放在需要访问的服务器上。如果你要连接到ssh服务器上
客户端软件就会向服务器发出请求，请求用你的密匙进行安全验证。
 
 2.ssh基于密码远程登录
  
  ssh ip地址 -- 登录到远程指定的机器上(需要输入用户名和密码)
  可以通过主机名登录
  需要配置主机名和ip地址的映射关系
  分贝得在centos7-1 centos7-2 centos7-3 中配置
  vi /etc/hosts
  接下来可以用主机名登录
  192.168.72.100 centos7-1
  192.168.72.101 centos7-2
  192.168.72.103 centos7-3
  ssh centos7-2
  
 3.ssh基于秘钥实现免密登录
  前提：配置hostname与ip映射
   1)在三台机器器执行行以下命令，生成公钥与私钥
     ssh-keygen -t rsa 在三台机器上执行以下命令，生成公钥与私钥
   2)将centos7-2 和 centos7-3 的公钥拷贝到centos7-1
   在centos7-1、centos7-2 和 centos7-3 上执行
   ssh-copy id centos7-1 -- 将公钥拷贝到centos7-1 上面去
   3)再将centos7-1 的公钥分发给centos7-2 和 centos7-3
   scp authorized_keys centos7-2:$PWD
   scp authorized_keys centos7-3:$PWD
   使用ssh完成免密登录即可
   ssh centos7-2
  
  4.三台机器时钟同步
    
	如何同步
	1)查看本机当前时间
	date
	2)设置本机当前时间
	date -s "2021-10-30 20:08:09"
	3)通过命名和时钟服务器同步时间
	网络计时协议(NTP) : net time protocal
	ntpdate us.pool.ntp.org
	yum -y install ntpdate
	4)编辑定时任务
	crontab -e
	定时任务如下
	*/1 * * * * /usr/sbin/ntpdate us.pool.ntp.org;
	每隔1分钟执行指令一次
	实例 每个星期一的上午8点到11点的第3和第15分钟执行
	命令 命令：3,15 8-11 * * 1 command
	Crantab在线生成
    https://cron.qqe2.com/
	
	
  
   
   
  
  