 1.shell编程简介
 
  Linux中的shell脚本是一个特殊的应用程序，它介于操作系统和系统内核之间，充当一个命令解释器的角色。负责接收用户输入
的操作指令并进行解释，将需要执行的操作传递给内核执行，并输出执行结果。同时它又是一种程序设计语言。作为命令语言，它
交互式解释和执行用户输入的命令或者自动地解释和执行预先设定好的一连串的命令;作为程序设计语言,它定义了各种变量和参数
,并提供了许多在高级语言中才具有的控制结构，包括循环和分支.
  Linux的Shell解释器种类众多，一个系统可以存在多个shell，可以通过cat /etc/shells 命令查看系统中安装的shell解释器。
  Bash由于易用和免费，在日常工作中被广泛使用。同时，Bash也是大多数Linux系统默认的Shell。
 
 2.shell解释器
  
   java需要虚拟机解释器,同理shell脚本也需要解析器。
   [root@centos7_1 shells]# cat /etc/shells
   /bin/sh
   /bin/bash
   /sbin/nologin
   /bin/dash
   /bin/tcsh
   /bin/csh
 
 3.编写脚本
   
   新建 /usr/shell/hello.sh 文件
   [root@centos7_1 shell]# vim hello.sh
   #!/bin/bash
   echo "hello lagou"
   chmod +x hello.sh
   !是一个约定的标记，它告诉系统这个脚本需要什么解释器来执行，即使用哪一种Shell。
   echo 命令用于向窗口输出文本
 
 4.执行shell脚本
 
   执行方式1: 
   [root@centos7_1 shell]# /bin/sh hello.sh
   [root@centos7_1 shell]# /bin/bash hello.sh
   [root@centos7_1 shell]# vim hello.sh
   问题:bash 和 sh什么关系
   sh 是 bash 的快捷方式
   
   执行方式2：
   [root@centos7_1 shell]# bash hello.sh
   [root@centos7_1 shell]# sh hello.sh
   1).查看环境变量path
   echo $PATH
   2).添加执行权限
   chmod +x 文件名称
 
 5.变量
   
   在shell脚本中,定义变量时，变量名不加美元符号($)，如:
   username="tom"
   注意:变量名和等号之间不能有空格，这可能和你熟悉的所有编程语言都不一样
   同时，变量名的命名须遵循如下规则:
   命名只能使用英文字母，数字和下划线，首个字符不能以数字开头
   中间不能有空格，可以使用下划线(_)。
   不能使用标点符号
   不能使用bash里的关键字(可用help命令查看保留关键字)
   有效的shell变量名实例如下:
   jack
   LD_LIBRARY_PATH
   _demo 
   var2 
   无效的变量命名:
   ?var=123
   user*name=runoob
   使用语句给变量赋值
   for file in ls /etc
   或 for file in $(ls /etc) -- 以上语句是将/etc下目录的文件名循环出来
 
 6.使用变量
  
   使用一个定义过的变量，只要在变量名前面加美元符号即可，如：
   you_name="tom"
   echo $you_name
   echo ${you_name}
   变量名外面加花括号是可选的,添加花括号是帮助解释器识别变量边界
   举例
   habby="i like ${course}script"
   habby="i like $coursescript"
 
 7.只读变量
  
   使用readyonly命令可以将变量定义为只读变量,只读变量的值不能被改变
   readonly 变量名
  举例:
  username=tom
  readonly username
  username=jack 报错 bin/sh: NAME: This variable is readonly
 
 8.删除变量
 
  使用unset命令可以删除变量
  unset variable_name
  注意:unset命令不能删除只读变量。

 9.字符串
   
   字符串是shell编程中最常用最有用的数据类型(除了数字和字符串，也没啥其它类型好用了)，字符串
可以用单引号。也可以用双引号，也可以不用引号。
   单引号
   shill='java'
   str='I am goot at $skill'
   echo $str
   输出结果
   I am goot at $skill
   单引号字符串的限制
   单引号里的任何字符都会原样输出，单引号字符串中的变量是无效的。
   单引号字串中不能出现单独一个的单引号(对单引号使用转义符后也不行)
   双引号
   shill="java"
   str="I am goot at $skill"
   echo $str
   输出结果
   I am goot at java
   双引号的优点
   双引号里可以有变量。
   双引号里可以出现转义字符
 
 10.获取字符串长度
   
   skill='java'
   echo ${skill} -- 输出结果 java 
   echo ${#skill} -- 输出结果 4
 
 11.提取字符串
   
   一下实例从字符串第2个字符开始截取4个字符
   str="i like hodoop"
   echo ${str:2} -- 相当于subtring(2)
   echo ${str:2:2} -- 相当于subtring(2,2)
 
 12.查找子字符串
  
   查找字符o在那个位置(最先出现的字符)
   str="hadoop is so easy"
   echo expr index "$str" o
   找的时候是从1开始查找
 

   
      
   
   
   
   