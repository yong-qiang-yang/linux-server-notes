 1.date显示当前时间
 
   1).date -- 显示当前时间
   2).date +%Y -- 显示当前年份
   3).date +%m -- x显示当前月份
   4).date +%d -- 显示当前是哪一天
   5).date +%Y%m%d -- 显示当前年月日各种格式
   6).date "+%Y-%m-%d %H:%M:%S" -- 或者单引号也可以 显示年月日时分秒
   
   显示的是字符串串描述的时间，不是当前时间
  
 2.date显示非当前时间
 
   1)基本语法
    date -d '1 days ago' -- 显示前一天的日期
	date -d yesterday +"%Y-%m-%d" -- 同上
	date -d next-day +"%Y-%m-%d" -- 显示明天日期
	date -d 'next monday' -- 显示下一周的时间
 
 3.设置系统时间
 
   基本语法：date -s 字符串时间
   案例
   [root@hadoop106/]#date -s "2020-06-20 20:52:18"
  
  4.cal查看日历
   
   基本语法 cal [选项] -- 不加选项，显示本月日历
   选项：
   -3 -- 显示系统前一个月，当前月，下一个月的日历
   具体某一年，显示这一年年的日历
   案例
   [root@lagou/]# cal
   [root@lagou/]# cal -3
   [root@lagou/]# cal 2020
   
 5.find查找文件或者目录
  
   find命令是根据文件的属性进行查找，如文件名，文件大小，所有者，所属组，是否为空，访问时间，修改时间等。
   基本格式: find path [options]
   1).按照文件名查找
	find /etc -name yum.conf -- 在/etc目录下文件yum.conf
	find /etc -name 'yum' -- 使用通配符*(0或者任意多个)。表示在/etc目录下查找文件名中含有字符串'yum'的文件
	find . -name 'yum' -- 表示当前目录下查找文件名开头是字符串'yum'的文件
   2).按照文件特征查找
	find / -atime -2 -- 查找在系统中最后48小时访问的文件(AccessTime，文件读取访问时间)
	find / -empty -- 查找在系统中为空的文件或者文件夹
	find / -group susan -- 查找在系统中属于group为susan的文件
	find / -mtime -1 -- 查找在系统中最后24小时里修改过的文件(modifytime)
	find / -user susan -- 查找在系统中属于susan这个用户的文件
	find / -size +10000c -- 查找出大于10000字节的文件(c:字节，w:双字，k:KB，M:MB，G:GB)
	find / -size -1000k -- 查找出小于1000KB的文件
   3).使用混合查找方式查找文件
	find /tmp -size +10c -and -mtime +2 -- 在/tmp目录下查找大于10字节并在2天前修改的文件
	find / -user root -or -user susan -- 在/目录下查找用户是root或者susan的文件文件
	find /tmp ! -user susan -- 在/tmp目录中查找所有不属于susan用户的文件

 6.grep 过滤查找
 
  grep是根据文件的内容进行查找，会对文件的每一行按照给定的模式(patter)进行匹配查找。
  基本格式: grep [options] 范围
  [options] 主要参数
           -c：只输出匹配行的计数
		   -i：不区分大小写
		   -n：显示匹配行及行号
		   -w：显示整个单词
		   -r：递归查询
  实例练习
  1)在applicationContext.xml文件中查找name
    grep name applicationContext.xml
  2)查找name统计行号
    grep -n name applicationContext.xml
  3)统计jdbc的个数
    grep -c jdbc applicationContext.xml
  4)查找dataSource并忽略大小写
    grep -i dataSource applicationContext.xml
  5)查找mapper单词
    grep =iw mapper applicationContext.xml
  6)递归查询/usr目录下含有name的字段
    grep -r name /usr
	
	
  
   