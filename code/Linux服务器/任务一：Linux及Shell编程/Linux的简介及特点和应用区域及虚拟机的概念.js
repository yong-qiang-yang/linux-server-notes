 1.了解Unix系统
 
  Unix是较早被广泛使用的计算机操作系统之一，它的第一版于1969年由Ken Thompson在AT&T贝尔实验室是实现。1973年Ken Thompson与
Dennis Ritchie用C语言重写了Unix的第三版内核.

 2.Unix的特点
 
  Unix是一个强大的多用户、多任务操作系统。
  UNIX的商标权由国际开放标准组织(The Open Group)所拥有。
  UNIX操作系统是商业版，需要收费，价格比Microsoft Windows正版要贵一些。
 
 3.了解Linux发展历史
 
  Linux是一套自由加开放源代码的类Unix操作系统，诞生于1991年10月5日(第一次正式向外公布)由芬兰学生Linus Torvalds和后来陆续
加入的众多爱好者共同开发完成。
  Linux这个词本身只表示Linux内核，但实际上人们已经习惯了用Linux来形容整个基于Linux内核，并且使用GNU工程各种工具和数据库
的操作数据库。
  Richard M.Stallman 是自由软件之父
 
 4.Linux系统特点
 
  开放性（开源）、多用户、多任务、良好的用户界面、优异的性能和稳定性以及多用户多任务的特点。
  多用户：多个用户，在登陆计算机（操作系统），允许同时登陆多个用户进行操作
  多任务：多个任务，允许用户同时进行多个操作任务。
  注意：Windows属于单用户多任务，Linux属于多用户多任务。
 
 5.Linux的应用领域
 
  服务器系统:Web应用服务器、数据库服务器、游戏服务器、接口服务器、DNS、FTP等等。
  嵌入式系统:路由器、防火墙、手机、PDA、IP分享器、交换器、家电用品的微电脑控制器等等。
  高性能运算、计算密集型应用：Linux有强大的运算能力。IBM的Watson超级计算机就是使用了Linux系统。
  桌面应用系统：很多桌面操作系统的底层也是Linux。
  移动手持系统：安卓系统就是基于Linux。
 
 6.Linux版本
 
  Linux的版本继承了Unix的版本定制规则，分为内核版本和发行版本。
  内核版本：内核就是一个核心，其他软件都基于这个核心，不能直接使用，内核版本统一在http://www.linux.org 发布。
  发行版本由各个Linux发行商发布，Linux发行商有权选择Linux的内核版本。常见的Linux的发行版本：RedHat、CentOS、Debian、
Ubuntu。
  内核版本分为稳定版和开发版，区分方式是根据次版本的奇偶判定，奇数为开发版，偶数为稳定版。
  
  Debian:Debian运行起来极其稳定，这使得它非常适合用于服务器。
  redhat:这是第一款面向商业市场的Linux发行版。它有服务器版本，支持众多处理器架构。
  全球最大的linux发行厂商，功能全面、稳定。
  ubuntu:Ubuntu是Debian的一款衍生版，侧重于它在这个市场的应用，在服务器、云计算、甚至一些运行的移动设备上很常见。
  centos:CentOS是一款企业级Linux发行版，它使用红帽企业级Linux中的免费源代码重新构建而成。这款重构版完全去掉了注册
商标以及Binary程序包方面一个非常细微的变化。
  Fedora:Fedora同是一款非常好的发行版，有庞大的用户论坛，软件库中还有为数不少的软件包。Fedora同样使用YUM来管理软件包。

 7.什么是虚拟
  
  虚拟机(Virtual Machine)指通过软件模拟的具有完整硬件系统功能的、运行在一个完全隔离环境中的完整计算机系统。
  常用的虚拟机
  常用的虚拟机软件
  主要包括：VMware Workstation、VirtualBox、Virtua* PC
  VMware Workstation:是VMware公司销售的商业软件产品之一。该工作站软件包含一个用于英特尔x86相容电脑的虚拟机套装，其允
许用户同时创建和运行多个x86虚拟机。
  VirtualBox: VirtualBox是一款开源虚拟机软件。VirtualBox是由德国Innotek公司开发，由Sun Microsystems公司出品的软件，使
用Qt编写，在Sun被Oracle收购后正式更名成Oracle VM VirtualBox。
  irtua* PC：是Microsoft最新的虚拟化技术。主要适合做微软自己产品的服务。
  
  
  




 
  

