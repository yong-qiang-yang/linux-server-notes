 1.Linux的组成
 
  内核：是系统的心脏，是运行程序和管理像磁盘和打印机等硬件设备的核心程序。
  Shell：是系统的用户界面，提供了用户和内核进行交互操作的一种接口。它接收用户输入的命令并把它送入内核去执行，是一个命令解释器
。但它不仅是命令解释器，而且还是高级编程语言，shell编程。
  FILE SYSTEMS(文件系统)：文件系统是文件存放在磁盘等存储设备上的组织方法，Linux支持多种文件系统，如ext3,ext2,NFS,SMB,iso9660等。
  应用程序：标准的Linux操作系统都会有一套应用程序例如X-Window,Open Office等。
  
  2.Linux目录结构
   
   /boot：系统引导文件、内核
   /bin：用户的基本命令
   /dev：设备文件
   /etc：配置文件
   /home：用户目录
   /root：root用户目录
   /sbin：管理类的基本命令
   /tmp：临时文件存放地
   /usr：共享的只读数据
   /mnt：临时文件系统挂载点
   /media：移动设备挂载点
   /opt：第三方应用程序的安装位置
   /srv：系统运行的服务用到的数据
   /var：变化的数据文件
   /proc：用于输出内核与进程信息相关的虚拟文件系统
   /sys：用于输出当前系统上硬件设备相关信息的虚拟文件系统
  
 3.常用Linux命令的常用快捷键
  
   1)tab键：命令或者路径提示及补全;
   2)ctrl+c：放弃当前输入，终止当前任务或程序；
   3)ctrl+l：清屏；
   4)ctrl+insert：复制；
   5)鼠标右键：粘贴；
   6)alt+c：断开连接/ ctrl+shfit+R 重新连接
   7)alt+1/2/3/4/5…： 切换会话窗口
   8)上下键：查找执行行过的命令,或者是history命令
  
 4.终端命令格式
   
   command [-options] [parameter]
   说明 command:命令名,相应功能的英文单词或单词的缩写
        [-options]:选项,可用来对命令进行控制,也可以省略
        parameter:传给命令的参数,可以是零个、一个或者多个

 5.帮助命令
   
   因为一个命令有很多可选项,死记硬背肯定不行,所以需要借助手册查阅。
   1)-- help帮助信息
   command --help 说明 显示command命令的帮助信息
   缺点:虽然可以查询命令的帮助信息,但是没有提供翻页、搜索功能
   2)man手册
   man command   说明 查询command命令的使用手册
   man 是 manual 的缩写,是Linux提供的一个手册,包含了绝大部分的命令、函数的详细使用说明。
   使用man 时的操作键:
   操作键                            功能
   空格键                      显示手册的下一行  
   Enter键                   一次滚动首页也得一行
   b                               回滚一屏
   f                               前滚一屏
   q(quit)                           退出
   /word                        搜索word字符串
   n(next)                        搜索下一个
   N                              搜索上一个
   提醒
   现阶段只需要 知道 通过以下两种方式可以查询命令的帮助信息
   先学习常用命令及常用参数的使用即可,工作中如果遇到问题可以借助网络搜索
 
 6.其他常用命令
   
   ls(list)  -- 查看当前目录下的内容
   pwd(print working derectory)  -- 查看当前所在文件夹
   cd(change directory) [目录名]  -- 切换文件夹
   touch [文件名]                 -- 如果文件不存在,新建文件
   mkdir(make directory) [目录名] -- 创建目录
   rm(remove) [文件名]            -- 删除指定的文件名
   clear                          -- 清屏
 
 7.目录操作命令
  
  pwd: 查看当前所在路径
  cd: 切换目录
    cd .. : 切换到上级目录
	cd -: 后退到上一次所在目录
	cd /:去根目录
	绝对路径: /开始的目录，从根目录开始
	相对路径: 直接目录，从当前目录开始
  练习                               答案
  1)查看当前所在目录                 pwd
  2)切换到 /usr/local(绝对路径)      cd /usr/local
  3)切换到 上一级 /usr               cd ..
  4)切换到 /usr/tmp(相对路径)        cd tmp
  5)切换回 /usr/local                cd /usr/local
  6)后退到上一次所在目录             cd -
 
 8.查看目录内容
  
   ls: 查看目录下内容
   ls -a: 查看全部内容，包含隐藏文件
   ls -l: 查看内容的详细信息，效果等同于 ll命令
   ls -lh: 以人能读懂的方式显示文件大小
   ls 练习                                                  答案
   1) 查看 /usr内容                                         ls
   2) 查看所有 /usr内容(既包含隐藏，也包含非隐藏)           ls -a
   3) 查看 /usr详细内容                                     ls -l
   4) 简化 查看 /usr详细内容                                ll
   5) 易懂简化版 查看 /usr 详细内容                         ls -lh
   扩展: linux 如何树结构显示某文件夹下的所有文件(包含子文件)？ ls -R
 
 9.创建文件
  
   通过 touch 命令 创建文件
   1).创建一个空文件夹
   touch 不存在的文件
   2).修改文件的末次访问时间
   touch 存在的文件
   案例
   [root@lagou ~] touch test.txt
  
 10.创建目录命令
 
  通过 mkdir 命令创建目录
  基本语法 mkdir [-p] 要创建的目录
  -p(parent) -- 可以递归创建目录
  案例
  [root@lagou ~]# mkdir test
  [root@lagou ~]# mkdir -p test/test1
  注意
  通过 mkdir -p 目录名 命令 创建目录
  注意:新建目录的名称 不能与当前目录中 已有的目录或文件同名
  
 11.rmdir删除一个空的目录
  
  基本语法：
  rmdir 要删除的空目录，remove 即移除
  案例
  [root@lagou ~]# mkdir test2
  [root@lagou ~]# rmdir test2
  注意：如果该目录中存在文件或其他目录是该命令是不能删除的。
 
 12.touch创建文件和rm 删除文件
 
   1).创建文件的命令
   touch 文件名称
   注意事项: touch 文件名1 文件名2 .. 可以创建多个文件
   2).删除命令
   语法
   rm [-参数] 文件/目录 (功能描述: 递归删除目录中所有内容) 慎用
   通过rm -rf 文件/目录 命令实现删除 文件和目录的功能 rm(remove) -- 删除
   -f(force) -- 强制删除,忽略不存在的文件,无需提示
   -r(recursive) -- 递归地删除目录下的内容,删除文件夹 时必须加此参数
  
 13.文件操作命令
 
   touch 文件名：创建文件
   rm: 删除文件或目录
     rm 文件名: 删除一个文件
	 rm -f 文件名: 不经确认就删除文件
	 rm -r 目录: 递归删除一个目录及目录中的内容
	 rm -rf 目录: 递归删除一个目录，并且不经确认
	 rm -rf * : 清空当前文件夹
	 rm -rf /*  : 自杀行为，不要尝试      */
	 
   案例
      1) 删除目录
	  [root@lagou test]# rmdir test1
	  2) 递归删除目录中所有内容
	  [root@lagou test]# rm -rf test2
	
 14.cp 复制拷贝命令
 
    通过 cp 实现复制将指定的 文件 或 目录 复制到 两一个 文件 或 目录中
	基本语法
	1). cp source dest -- 功能描述：复制source文件到dest
	2). cp -r sourceFolder targetFolder -- 功能描述: 递归复制整个文件夹
	-r(recursive)  -- 递归复制目标目录的内容
	案例
	1).复制文件
    [root@lagou opt]# cp test.txt test1.txt
    2).递归复制整个文件夹
	[root@lagou opt]# cp -r abc /tmp

 15.mv(move)
 
  通过 mv 命令可以用来 移动 文件 或 目录,也可以给 文件或目录重命名
  基本语法
  (1). mv oldNameFile newNameFile (功能描述：重命名)
  (2). mv /temp/movefile /targetFolder (功能描述：递归移动文件)
   案例
   1).重命名 
   [root@lagou test]# mv file1 file11 (把file1文件夹改为file11)
   2).移动文件
   [root@lagou test]# mv file11 test  (把file11文件夹放到test文件夹内)
   
 16.cat查看文件内容
   
   查看文件内容，从第一行行开始显示
   1).基本语法
   cat [选项] 要查看的文件
   2).选项
   -b：列出行号，仅针对非空白行做行号显示，空白行不标行号！
   -E：将结尾的断行行字节$显示出来；
   -n：列出行号，连同空白行也会有行号，与-b的选项不不同；
   -T：将[tab]按键以^I显示出来；
   -v：列出一些看不出来的特殊字符
   -A：相当于-vET的整合选项，可列列出一些特殊字符而不不是空白而已；
 
 17.more 查看文件内容
 
 查看文件内容，一页一页的显示文件内容
 1).基本语法 more 要查看的文件
 2).功能使用说明
 空格键 (space)：代表向下翻一页
 Enter:代表向下翻『一行』
 q代表立刻离开more，不再显示该文件内容
 ctrl+F 向下滚动一屏
 ctrl+B 返回上一屏
 = 输出当前行
 3).案例
 [root@lagou test]# more test1.java
 
 18.less 查看文件内容
 
  less的作用与 more十分相似，都可以用来浏览文字档案的内容，不同的是less允许使用[pageup][pagedown]往回滚动
  1).基本语法 less 要查看的文件
  2).功能使用说明
  空格键：向下翻动一页  
  [pagedown]：向下翻动一页
  [pageup]：向上翻动一页
  /字符串：向下搜寻『字符串』的功能；n：向下查找；N：向上查找
  q：离开 less 这个程序
  3).案例
 [root@lagou test]# less test1.java
 
 19.head查看文件内容
 
   查看文件内容，只看头几行，优点：对于大文件不必都加载，只显示头几行行即可
   1).基本语法
   head 文件名 ：查看前10行
   head -n 3 文件名 :查看文件的前3行
   head -c 3 文件名 :查看文件的前3个字符
 
 20.tail 查看文件内容
 查看文件内容，只看尾巴几行行，优点：可以查看?文件实时追加的内容。
 1).基本语法
 tail -n 10 文件 (功能描述：查看文件头（从末尾开始数）10行行内容，10可以是任意行行数)
 tail -f 文件 (功能描述：实时追踪该文档的所有更更新)
 2).案例                                              答案
 (1). 查看文件后10行内容                           tail -10 文件名  
 (2). 动态追踪文件内容                             tail -f 文件名
 (3). 动态追踪 最后10行内容 且 退出             tail -10f 文件名 (ctrl+c 是退出)
 

  
	 
   
  
   