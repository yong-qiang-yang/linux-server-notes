 1.打tar包
 
  类似将 冬天的衣服 放到 袋
  打包之后的大文件 需要以.tar 结尾
  tar 打包命令格式
  # 将 一系列 打包成 一个大文件
  tar -cvf 打包名.tar 被打包的目录
  tar -cvf 打包名.tar 被打包的文件1 被打包的文件2 被打包的文件3
  c(create) -- 生成档案文件,创建打包文件
  v(verbosely) -- 像'唐僧'一样报告进度
  f(file) -- 指定档案的文件名称, f后面一定是.tar文件,所以必须放到左后
  练习1:将1.txt、2.txt、3.txt打包成123.tar文件
  tar -cvf test2.tar 1.txt 2.txt 3.txt
  
 2.解tar包
  
  类似将 冬天的衣服 从 袋子里取出来
  tar 解包命令格式
  # 将一个打包后的 分解成 一系列小文件，分解位置为 当前目录
  tar -xvf 打包名.tar
  # 将一个打包后的 分解成 一系列小文件，分解位置为 指定目录
  x(extract：提取) -- 解包
  C(大写C)(directory) -- 默认保存到当前目录,通过-C更改解压目录,注意:解压目录必须存在
  
  练习1: 将 123.tar 解压到当前目录中
  [root@localhost tmp]# tar -xvf 123.tar
  练习2: 将 aaa.tar 解压都 /export/test/a1/b1/c1 目录中
  [root@localhost tmp]# tar -xvf test2.tar -C /export/test/a1/b1/c1
  [root@localhost tmp]# cd /export/test/a1/b1/c1
  [root@localhost tmp]# ll
  小结
  打包: tar -cvf 打包之后的文件名.tar 被打包的目录或文件名
  解包: tar -xvf 打包之后的文件名.tar [-C指定解包位置]

 3.gzip格式压缩和解压缩
  
  在 Linux 中,最常用的压缩文件格式是xxx.tar.gz
  在 tar 命令中有一个选项 -z可以调用gzip,从而可以方便的实现压缩和解压缩的功能
  命令格式如下：
  压缩文件: tar -zcvf 打包压缩文件名.tar.gz 被压缩的文件/目录中
  解压缩文件: tar -zxvf 打包文件.tar.gz
  解压缩到指定路径
  tar -zxvf 打包文件.tar.gz -C 目录路径
  z(gzip2) -- 使用gzip压缩和解压缩
  
  练习1：将1.txt、2.txt、3.txt 打包压缩成 123.tar.gz文件(gzip压缩模式)
  tar -zcvf 123.tar.gz 1.txt 2.txt 3.txt
  练习2: 将123.tar.gz 解压到当前目录中(gzip压缩模式)
  tar -zxvf 123.tar.gz
  练习3：将aaa.tar.gz 解包到 /export/bbb 目录中(gzip压缩模式)
  tar -zxvf aaa.tar.gz -C /export/bbb
  
 4.bzip2格式压缩和解压缩
 
  在 Linux 中, bzip2压缩文件格式是xxx.tar.bz2
  在 tar 命令中有一个选项-j可以调用 bzip2 ,从而可以方便的实现压缩和解压缩的功能
  命令格式如下：
  压缩文件: tar -jcvf 打包压缩文件名.tar.bz2 被压缩的文件/目录中
  解压缩文件(绩效潍坊): tar -jxvf 打包文件.tar.bz2
  解压缩到指定路径
  tar -jxvf 打包文件.tar.bz2 -C 目录路径
  注意事项：如果报错tar(child):bzip2：无法exec:没有那个文件或目录要安装bzip2的包
  yum install -y bzip2
  j(bzip2) -- 使用bzip2压缩和解压缩
  
  练习1：将1.txt、2.txt、3.txt 打包压缩成 123.tar.bz2文件(gzip压缩模式)
  tar -jcvf 123.tar.bz2 1.txt 2.txt 3.txt
  练习2: 将123.tar.bz2 解压到当前目录中(gzip压缩模式)
  tar -jxvf 123.tar.bz2
  练习3：将aaa.tar.bz2 解包到 /export/bbb 目录中(gzip压缩模式)
  tar -jxvf aaa.tar.bz2 -C /export/bbb
  打包压缩: tar -jcvf 打包之后的文件名.tar.bz2 被打包压缩的目录或文件名
  解包解压缩: tar -jxvf 打包之后的文件名.tar.bz2 [-C 指定解包位置]
  
  
  