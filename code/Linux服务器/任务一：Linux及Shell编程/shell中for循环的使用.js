 1.for循环
 
  格式
  for variable in (list); do
  command
  command
  ..
  done
  练习
  [root@lagou shell]# vim test_for.sh
  #!/bin/bash
  # 需求1:遍历 1~5
  for i in 1 2 3 4 5 ; do
           echo $i;
  done
  
  echo "------------------------------------"
  [root@lagou shell]# chomd +x test_for.sh
  [root@lagou shell]#./test_for.sh
  # 需求2:遍历 1~100
  [root@lagou shell]# vim test_for.sh
  for i in {1..100}; do
           echo $i;
  done
  
  echo "------------------------------------"
  [root@lagou shell]#./test_for.sh
  # 需求3:遍历 1~100之间的奇数
  [root@lagou shell]# vim test_for.sh
  for i in {1..100..2}; do
           echo $i;
  done
  
  echo "------------------------------------"
  [root@lagou shell]#./test_for.sh
  # 需求4:遍历 根目录 下的内容
  [root@lagou shell]# ll /
  [root@lagou shell]# vim test_for.sh
  for f in `ls /`; do
         echo $f
  done
  
  echo "------------------------------------"
  [root@lagou shell]#./test_for.sh
  
  