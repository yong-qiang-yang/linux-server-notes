 1.while语句
 
  while循环用于不断执行一系列命令，也用于从输入文件中读取数据；命令通常为测试条件。其格式为
  while condition; do
      command
  done
  
  
  [root@lagou shell]# vim test_while.sh
  #!/bin/bash
  #需求: 计算1~100 的和
  sum=0;
  i=1;
  while [ $i -le 100 ] ; do
          sum=$[ sum + i ]
		  i=$[ i + 1]
  done
  echo $sum
  [root@lagou shell]# chmod +X test_while.sh
  [root@lagou shell]#./test_while.sh
 
 2.无限循环
 
  for ((;;))
  do 
      command
  done
  
  while true
  do
      command
  done
  [root@lagou shell]# vim test_for_while.sh
  #!/bin/bash
  #需求：使用for每过1秒打印hello
  for ((;;))
  do 
      echo "hello"
	  sleep 1
  done
  [root@lagou shell]# chmod +X test_for_while.sh
  [root@lagou shell]#./test_for_while.sh
  #需求：使用while循环每过1秒打印当前时间
  while true
  do 
       echo `date +"%Y-%m-%d %H:%M%S"`
	   sleep 1
  done
  [root@lagou shell]#./test_for_while.sh
 
 3.case
   
   Shell case语句为多选择语句。可以用case语句匹配一个值与一个模式，如果匹配成功，执行相匹配的命令。
   case语句格式如下
   case 值 in
        模式1)
		   command1
		   command2
		   ...
		   commandN
		   ;;
		模式2)
		   command1
		   command2
		   ...
		   commandN
		   ;;
   esac
   case工作方式如上所示。取值后面必须为单词in，每一模式必须以右括号结束。取值可以为变量或常数。匹配发现取值符合某
模式后，其间所有命令开始执行直至;;
   取值将检测匹配的每一个模式。一旦模式匹配，则执行完匹配模式相应命令后不再继续其他模式无一匹配模式，使用星号*捕
该值，再执行后面的命令。
   下面的脚本提示输入1到4，与每一种模式进行匹配
   [root@lagou shell]# vim test_case.sh
   #!/bin.bash
   
   echo "请输入1-4的数字"
   read anum
   echo $anum
   case $anum in
            1) echo "你选择了1"
			;;
			2) echo "你选择了2"
			;;
			3) echo "你选择了3"
			;;
			4) echo "你选择了4"
			;;
			*) echo "你没有选择1-4的数字"
			;;
   esac
   [root@lagou shell]# chmod +X test_case.sh
   [root@lagou shell]# ./test_case.sh
   输入不同的内容，会有不同的结果，例如
   请输入1-4 的数字
   你输入的数字为:
   3
   你选择了3
 
 4.跳出循环
  
   在循环过程中，有时候需要在未达到循环结束条件时强制跳出循环，Shell使用两个命令来实现该功能break和continue。
   1).break命令
   break命令允许跳出所有循环(终止执行后面的所有循环)
   需求:执行死循环，每隔1秒打印当前时间,执行10次停止
   [root@lagou shell]# vim test_break.sh
   #!/bin/bash
   #需求：执行死循环，每隔1秒打印当前时间,执行10次
   i=0
   while true ;do
         echo `date +"%Y-%m-%d %H:%M%S"`
		 sleep 1
		 i=$[ i + i ]
		 if [ $i -eq 10 ]; then
		            break
		 fi
  done
  [root@lagou shell]# chmod +x test_break.sh
  [root@lagou shell]# ./test_break.sh
  2).continue
  continue命令与break命令类似，只有一点差别，它不会跳出所有循环，仅仅跳出当前循环。
  需求:打印1-30 ，注意 跳过3的倍数
  [root@lagou shell]# vim test_continue.sh
  #!/bin/bash
  #需求:打印1-30,注意 跳过3的倍数
  for i in {1..30}; do
       result=$[ i % 3 ]
	   if [ $result -eq 0 ]; then
	          continue
	   fi
	   echo $i
  done
  [root@lagou shell]# chmod +x test_continue.sh
  [root@lagou shell]# ./test_continue.sh
  
 5.函数使用
 
  函数的快速入门格式:
  [ function ] funname()
  {
	  action;
	  [return int;]
  }
  可以带function fun()定义，也可以直接fun()定义，不带任何参数
  参数返回，可以显示加：return返回，如果不加，将以最后一条命令运行结果，作为返回值return后
  [root@lagou shell]# vim test_fun.sh
  #!/bin/bash
  #定义函数
  fun1 () {
	   echo "我的第一个函数"
  }
  
  #调用函数
  fun1
  [root@lagou shell]# chmod +x test_fun.sh
  [root@lagou shell]# ./test_fun.sh
  #调用函数并且传递参数
  
  fun1 () {
	   echo "我的第一个函数"
	   #获取并打印参数
	   echo "参数1:$1"
	   echo "参数2:${2}"
	   echo "参数1:$1"
	   echo "获取所有的参数: $*"
	   echo "获取所有的参数: $@"
  }
  fun1 aa bb cc
  [root@lagou shell]# ./test_fun.sh
  
  