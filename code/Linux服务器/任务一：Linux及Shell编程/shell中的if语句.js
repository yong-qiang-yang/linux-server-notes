 1.if语句
 
  if语句格式
  if condition
  then
     command1
	 command2
	 ...
	 commandN
  fi
  
  if.else语句
  if condition
  then
     command1
	 command2
	 ...
	 commandN
  else
	 command
  fi
  
  if..elif..else
  if condition1
  then
     command1
  elif condition2
  then
     command2
  else
	  command
  fi
  案例
  [root@lagou shell]# vim test_if.sh
  #!/bin/bash
  #score=40
  score=95
  if [ $score -gt 60 ] ; then
        echo "及格"
  else  
	    echo "不及格"
  fi
  [root@lagou shell]# chmod +x test_if.sh
  [root@lagou shell]# ./test_if.sh
  [root@lagou shell]# vim test_if.sh
  if [ $score -ge 90 ] ; then
        echo "A"
  elif [ $score -ge 80 ] ; then
        echo "B"
  elif [ $score -ge 70 ] ; then
        echo "C"
  elif [ $score -ge 60 ] ; then
        echo "D"
  else  
	    echo "E"
  fi
  [root@lagou shell]# ./test_if.sh
    