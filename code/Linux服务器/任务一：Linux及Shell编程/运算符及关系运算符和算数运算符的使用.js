 1.运算符
 
  Shell和其他编程一样，支持包括：算术、关系、布尔、字符串等运算符。原生bash不支持简单的数学运算，但是可以通过其他命令来实现
，例如expr。expr是一款表达式计算工具，使用它能完成表达式的求值操作。
  例如，两个数相加
  val='expr 1 + 2'
  echo $val
  注意：
  表达式和运算符之间要有空格，例如1+2 是不对的，必须写成1 + 2
  完整的表达式要被 包含， 注意不是单引号，在Esc键下边。
  下表列出了常用的算术运算符，假定变量a为10，变量b为20
 
 2.算数运算符
 
   +(加法) -- expr $a + $b 结果为 30
   -(减法) -- expr $a - $b 结果为 10
   *(乘法) -- expr $a \* $b 结果为 200
   /(除法) -- expr $a / $b 结果为 2
   %(取余) -- expr $a % $b 结果为 0
   =(赋值) -- a=$b 将把变量b 的值赋给 a
   ==(相等，用于比较两个数字，相同则返回true) --  [$a == $b ] 返回false
   !=(相等，用于比较两个数字，不相同则返回true) -- [$a == $b ] 返回true
   注意:条件表达式要放在方括号之间，并且要有空格，例如:[$a==$b]是错误的
   必须写成[ $a == $b ]
   案例
   #!/bin/bash
   a=4
   b=20
   echo expr $a + $b -- 加法运算
   echo expr $a - $b -- 减法运算
   echo expr $a \* $b -- 乘法运算
   echo expr $a / $b -- 除法运算
   此外，还可以通过(())、$(())、$[]进行算术运算
   ((a++))
   echo "a = $a"
   c=$((a + b))
   d=$[a + b]
   echo "c = $c"
   echo "d = $d"
 
 3.关系运算符
   
   关系运算符只支持数字，不支持字符串，除非字符串的值是数字
   下表列出了常用的关系运算符，假定变量a为 10， 变量b为20:
   -eq(检查两个数是否相等，相等返回true)
   equals -- [$a -eq $b] 返回false
   -ne(检查两个数是否不相等，不相等返回true)
   not equals -- [$a -ne $b] 返回true
   -gt(检查左边的数是否大于右边的，如果是，则返回true)
   greater than -- [$a -gt $b] 返回false
   -lt(检查左边的数是否小于右边的，如果是，则返回true)
   less than -- [$a -lt $b] 返回true
   -ge(查左边的数是否大于等于右边的，如果是，则返回true)
   Greater than or equal to -- [$a -ge $b] 返回false
   -le(检查左边的数是否小于等于右边的，如果是，则返回true)
   less than or equal to -- [$a -le $b] 返回true
   
   