 1.组合命令
 
   Linux中的命令组合后，可以产生神奇的效果
   append 追加
   replace 替换，覆盖
   > 重定向输出
   >> 重定向输出，又追加功能
   示例：
   cat /etc/password > a.txt -- 将输出定向到a.txt
   cat /etc/password >> a.txt -- 输出并且追加
   echo控制台输出的内容(类似sout)
   需求                                                                答案
   1)删除 /usr/tmp/目录下的所有内容                        [root@localhost tmp]# rm -rf *
   2)增加 1.txt文件,内容:文本1                             [root@localhost tmp]# touch 1.txt && ls 1.txt
                                                           [root@localhost tmp]# vi 1.txt && echo 文本1 && cat 1.txt
                                                              
   3)增加 2.txt文件,内容:文本2                             [root@localhost tmp]# touch 2.txt && ls 2.txt
                                                           [root@localhost tmp]# vi 2.txt && echo 文本2 && cat 2.txt
   4)将2.txt内容复制粘贴到3.txt                            [root@localhost tmp]# cat 2.txt > 3.txt && cat 3.txt                            
   5)将1.txt内容复制粘贴到3.txt(缺点:产生替换效果)         [root@localhost tmp]# cat 1.txt > 3.txt && cat 3.txt
   6)将2.txt内容复制追加粘贴到3.txt中                      [root@localhost tmp]# cat 2.txt >> 3.txt && cat 3.txt 
   7)将ifconfig内容追加到3.txt中                           [root@localhost tmp]# ifconfig >> 3.txt && cat 3.txt
 
 2.管道"|"
 
  管道符号|的作用是:将一个命令的输出作为另一个命令的输入。
  配合使用的命令
  ps(Process Status) 进程状态 ps -ef
  grep(Global Regular ExpressionPrint) 全局正则表达式版本(搜索)
  管道是Linux命令中重要的一个概念，其作用是将一个命令的输出作用另一个命令的输入
  示例
  ls --help | more 分页查询帮助信息
  ps -ef | grep java 查询名称中包含java的进程
  需求
  1).分页查询 ls的帮助信息(回车键 下一行，空格键 下一页，ctrl+c 退出)
  ls --help | more
  2).查询ls帮助信息中包含'递归' 的指令
  ls --help | grep '递归'
  
 3.逻辑控制&&
 
  命令间的逻辑控制
  命令直接使用&&连接，实现类似逻辑与的功能
  只有在&&左边的命令运行成功时，&&右边的命令才会被执行
  只要左边命令运行失败，后面的命令就不会被执行
  例如：
  cp 1.txt 2.txt && cat 2.txt
  因为 启动软件通常不会打印启动的日志信息,所以需要再打开对应的日志信息查看
  分 tail -100f catalina.out
  ./startup.sh
  使用合并指令
  ./startup.sh && tail -100f catalina.out
  缺点：比较麻烦 解决：使用&&指令就可以一步到位
  这个经常把一些命令组合使用，比如我们在启动tomcat后，再用tail命令查看日志。如果启动失败，则不查看
  ./startup.sh && tail -50f ../logs/catalina.out
  需求：                                                  答案
  1) 打印1.txt内容 且 打印2.txt内容             cat 1.txt && cat 2.txt
  2) 打印100.txt内容 且打印2.txt内容(没有)     cat 100.txt && cat 2.txt
  3) 启动tomcat 且打印日志信息             ./startup.sh && tail -50f ../logs/catalina.out
  
 4.history查看所敲命令历史
 
  1).基本语法 history
  2).案例 [root@lagou test]# history
  
  
