 1.useradd 添加新用户
 
  基本语法：useradd 用户名 -- 添加新用户
  案例 [root@lagou~]# useradd hadoop
 
 2.passwd 设置用户密码
   
  基本语法：passwd 用户名 -- 设置用户密码
  案例 [root@lagou~]# passwd hadoop
  
 3.id 判断用户是否存在
 
  基本语法：id 用户名
  案例 [root@lagou~]# id hadoop
 
 4.切换用户
 
  基本语法：
  su 用户名称 -- 切换用户，只能获得用户的执行行权限，不能获得环境变量
  su - 用户名称 -- 切换到用户并获得该用户的环境变量及执行权限
  案例 
  [root@lagou~]# su hadoop
  [root@lagou~]# su - hadoop
 
 5.userdel 删除用户
 
  基本语法:
  userdel 用户名 -- 删除用户但保存用户主目录
  userdel -r 用户名 -- 用户和用户主目录，都删除
  案例
  1).删除用户但保存用户主目录
  [root@lagou~]# userdel hadoop
  2).删除用户和用户主目录，都删除
  [root@lagou~]# userdel -r hadoop
 
 6.who 查看登录用户信息
 
  基本语法：
  whoami -- 显示自身用户名称
  who am i -- 显示登录用户的用户名
  who -- 看当前有哪些用户登录到了了本台机器器上
 
 7.设置Linux普通用户具有root权限即sudo的使用
 
  1)sudo命令
  sudo是linux系统管理指令，是允许系统管理员让普通用户执行一些或者全部的root命令的一个工具，如halt，reboot，su等等
。这样不仅减少了root用户的登录和管理时间，同样也提高了安全性。
  2)修改配置文件
  修改/etc/sudoers文件，找到下面一行，在root下面添加一行，如下所示
  vim /etc/sudoers
  Allow root to run any commands anywhere
  root ALL=(ALL) ALL
  tom ALL=(ALL) ALL
  3)使用tom用户登录,操作管理员命令
  本质:使用临时管理员权限
  # 不切换root用户,也可以完成添加用户的功能
  sudo useradd lisi
  sudo passwd lisi

 8.cat /etc/passwd查看创建了哪些用户
 
  cat /etc/passwd
 
 9.用户组管理命令
  
  每个用户都有一个用户组，系统可以对一个用户组中的所有用户进行集中管理。不同Linux系统对用户组的规定有所不同，如linux
下的用户属于与它同名的用户组，这个用户组在创建用户时同时创建。
  用户组的管理涉及用户组的添加、删除和修改。组的增加、删除和修改实际上就是对/etc/group文件的更新
  
 10.groupadd 新增组
 
   基本语法：groupadd 组名
   案例: 添加一个hadoop组
   [root@lagou~]# groupadd hadoop
 
 11.groupdel 删除组
 
   基本语法：groupdel 组名
   案例: 删除一个hadoop组
   [root@lagou~]# groupdel hadoop
   
 12.groupmod 修改组
   
   基本语法：groupmod -n 新组名 老组名
   案例：修改hadoop组名称为hadoop1
   [root@lagou~]# groupmod -n hadoop1 hadoop
 
 13.cat /etc/group查看创建了哪些组
   
   cat /etc/group
 
 14.usermod 修改用户
  
  基本语法：usermod -g 用户组 用户名
  案例：将用户hadoop加入mygroup用户组
  [root@lagou~]# usermod -g mygroup hadoop
   
  
  
  