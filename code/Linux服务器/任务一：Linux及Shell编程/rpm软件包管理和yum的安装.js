 1.rpm软件包管理器
  
  1).目标
  通过 rpm命令 实现对软件 的安装、查询、卸载
  RPM 是Red-Hat Package Manager(RPM软件包管理器)的缩写
  虽然打上了red-hat的标记,但是理念开放,很多发行版都采用,已经成为行业标准
  2).路径
  第一步:rpm包的查询命令
  第二步:rpm包的卸载
  第三步:rpm包的安装
  3).实现
  (1).rpm包的查询命令
  -q(query) -- 查询
  -a(all) -- 所有
  -i(info) -- 信息
  -l(list) -- 显示所有相关文件
  -f(file) -- 文件，显示文件对应 rpm包
  查询已安装的rpm列表
  rpm -qa | grep xxx
  rpm -qa | less
  查询软件包信息
  rpm -qi 软件全包名
  查看一个rpm包中的文件安装到那里去了
  rpm -ql 软件全包名
  查看指定文件归属那个软件包
  rpm -qf 文件的全路径
  (2)rpm 包的卸载
  rpm -e(erase 清除) 软件包名称 -- 卸载rpm软件包
  rpm -e --nodeps(Don’t check dependencies) 软件包名称 -- 卸载前 跳过 依赖检查
  (3)rpm包的安装
   rpm -ivh rpm包的全路径   -- 安装rpm 包
   -i(install) -- 安装
   -v(verbose) -- 打印提示信息
   -h(hase) -- 显示安装进度
   小结
   1).查询: rpm -qa | grep rpm包
   2).卸载: rpm -e rpm全包名  
            rpm -e --nodeps rpm全包名
   3).安装：rpm -ivh rpm包的全路径
 
 2.yum介绍
   
    Yum(全称为 Yellow dog Updater, Modified)本质上也是一个软件包管理器
	特点:基于RPM包管理，能够从指定的服务器自动下载、自动安装、自动处理依赖性关系
 
 3.常用命令
    
	yum list | grep 需要的软件名 -- yum list显示所有已经安装和可以安装的程序
	yum -y install 需要的软件包  -- 下载安装
	yum -y remove 需要卸载的软件包 -- 卸载
	yum repolist -- 列出设定yum源信息
	yum clean all -- 清除yum缓存信息

 4.安装httpd软件
    
	安装httpd: yum -y install httpd
	启动httpd服务: service httpd start
	测试 http://192.168.72.100:80
	发现无法访问
	原因:因为linux的防火墙 禁止他人 访问自己的80端口
	解决: 通知 防火墙放行
	关闭防火墙
	systemctl stop firewalld 查看防火墙状态
	