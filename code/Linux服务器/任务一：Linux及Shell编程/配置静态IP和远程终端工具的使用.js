 1.配置静态ip
  
   配置网络的目的主要是为了固定虚拟机的内网IP，方便我们在真实的操作系统中使用Linux连接工具软件进行远程连接。
   1).关闭虚拟机
   2).点击编辑，选择倒数第二个选项"虚拟网络编辑器器",出现下面的"虚拟网络编辑器器"窗口。选择NAT模式
     注意子网IP前三位与NAT设置的网关IP、DHCP网段一致。
   3).进入操作系统配置网卡信息
     执行命令
	 vi /etc/sysconfig/network-scripts/ifcfg-ens33 点击i进行插入
	 BOOTPROTO=static
	 IPADDR=192.168.70.100
	 NETMASK=255.255.255.0
	 GATEWAY=192.168.70.2
	 DNS1=8.8.8.8
	 保存退出 esc退出 :wq 保存退出
	4).重启执行命令重启网卡服务
	syatemctl restart network
	5).检查IP是否更变
	命令：ip addr
	6).测试是否网络是否联通
	ping www.baidu.com

 2.远程终端命令工具
   
   主要功能是向Linux系统远程发送命令。
   Xshell：目前最好用
   SecureCRT:收费
   Putty:早就停止维护了，很多东西支持的很差。但因为习惯依旧很多人支持。
 
 3.XManager工具
  
  1)安装过程比较简单下一步
  2)配置主机地址
  3)输入用户名和密码
  4)xshell工作样式配置
  文件-> 外观 ——>配色方案：选择New White 字体大小12
  5)xftp工具使用
  xftp window操作系统和centos系统传输文件

  4.
	 