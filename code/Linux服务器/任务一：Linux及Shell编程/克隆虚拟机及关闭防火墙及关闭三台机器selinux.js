 1.克隆操作
   
   关闭当前centos7-1
   右键点击克隆在克隆中选择下一步选择"创建完整克隆"
   下一步修改虚拟机名称和位置点击完成即可。
  
 2.配置克隆后机器
  
   网络配置文件ifcfg-ens33的IP地址
   vi /etc/sysconfig/network-scripts/ifcfg-ens33
   ipaddr=192.168.72.101   
   重启网卡服务
   systemctl restart network
   检查ip地址是否更变
   ip addr
   hostname主机名，不要跟原克隆机一致
   查看主机命令: hostnamectl
   更改主机名：hostnamectl set-hostname centos7_2
   重启机器 reroot
 
 3.关闭三台虚拟机防火墙
   
   1)查看防火墙状态
   systemctl status firewalld
   2)设置防火墙停用状态
   syatemctl stop firewalld
   3)设置防火墙功能失效，开机自动关闭
   syatemctl disable firewalld
 
 4.三台机器关闭selinux
 
   安全增强型Linux(Security-Enhanced Linux)简称SELinux，它是一个Linux内核模块，也是Linux的一个安全子系统
   关闭安全策略否则SELinux服务开启后导致SSH连接异常
   修改文件
   vi /etc/selinux/config
   #selinux=enforcing
   selinux=disable
 
 
   